# -*- coding: utf-8 -*-

from openerp import models, fields, exceptions, api, _

PARKING_TYPE = [
    ('Premier', 'Montana'),
    ('Second', 'Saint-Paul'),
]

WORKFLOW_STATES = [
    ('draft', "Draft"),
    ('confirmed', "Confirmed"),
    ('done', "Done"),
]


class person(models.Model):
    _name = "parking.person"
    _rec_name = "lastname"
    firstname = fields.Char("Prénom", required="True")
    lastname = fields.Char("Nom", required="True")
    car_ids = fields.One2many("parking.car", "person_id")


class car(models.Model):
    _name = "parking.car"
    _rec_name = "model"
    model = fields.Char("Modèle", required="True")
    color = fields.Char("Couleur", required="True")
    parking_id = fields.Many2one("parking.parking")
    person_id = fields.Many2one("parking.person", ondelete="cascade")
    parking_type = fields.Selection(PARKING_TYPE, related="parking_id.parking_type")


class parking(models.Model):
    _name = "parking.parking"
    _rec_name = "parking_type"
    parking_type = fields.Selection(PARKING_TYPE, required="True")
    car_ids = fields.One2many("parking.car", "parking_id")
    max_parking_spots = fields.Integer(default=5)
    current = fields.Integer(compute="_set_current")

    @api.one
    @api.depends("max_parking_spots", "car_ids")
    def _set_current(self):
        self.current = self.max_parking_spots - len(self.car_ids)

    @api.one
    @api.onchange("current")
    def _verify_valid_spots(self):
        if self.current < 0:
            return {
                'warning': {
                    'title': "Parking plein",
                    'message': "Veuillez choisir un autre parking!",
                },
            }


class reservation(models.Model):
    _name = "parking.reservation"
    _rec_name = "car_id"
    car_id = fields.Many2one("parking.car")
    person_id = fields.Many2one("parking.person")
    parking_id = fields.Many2one("parking.parking")
    start_date_time = fields.Datetime()

    # WORK  FLOW TEST
    state = fields.Selection(WORKFLOW_STATES, default='draft')

    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_confirm(self):
        self.state = 'confirmed'

    @api.multi
    def action_done(self):
        self.state = 'done'
