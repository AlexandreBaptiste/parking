{
    'name': 'parking',
    'version': '0.1',
    'category': 'Generic Modules/Others',
    'description': """Test création module voiture""",
    'author': 'Alexandre BAPTISTE',
    'depends': ['base'],
    'data': ['views/parking_view.xml'],
    'installable': True,
    'auto_install': False,
}