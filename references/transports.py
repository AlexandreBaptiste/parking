# -*- coding: utf-8 -*-
from openerp import models, fields, exceptions, api, _
import openerp.addons.decimal_precision as dp
import requests


TRANSPORT_ORDER_STATE = [
    ('request', 'Request'),
    ('draft', 'Draft'),
    ('valid', 'Validated'),
    ('out_invoiced', 'Customer Invoiced'),
    ('in_invoiced', 'Supplier Invoiced'),
    ('done', 'Done'),
]

TRANSPORT_TYPE = [
    ('import', 'Import'),
    ('export', 'Export'),
]


TRANSPORT_TYPE_TO_TAX_STRATEGY = {
    'import': 'import',
    'export': 'export',
}

TRANSPORT_MISSION = [
    ('out', 'Out'),
    ('in', 'In'),
    ('delivery', 'Delivery'),
]

TRANSPORT_LOCATION_DISTANCE_METHODS = ['tro', 'gmaps']

GMAPS_DISTANCEMATRIX_URL = 'https://maps.googleapis.com/maps/api/distancematrix'


class ResCompany(models.Model):
    _inherit = 'res.company'

    transport_mission_disclaimer = fields.Text()


class ResPartner(models.Model):
    _inherit = 'res.partner'

    transport_request_ids = fields.One2many('erp_lodia.transport.order', 'partner_id',
                                          domain=[('state', '=', 'request')])
    transport_order_ids = fields.One2many('erp_lodia.transport.order', 'partner_id',
                                          domain=[('state', '!=', 'request')])

    transport_mission_ids = fields.One2many('erp_lodia.transport.mission', 'partner_id')
    transport_valid_mission_ids = fields.One2many('erp_lodia.transport.mission', 'partner_id',
                                                  domain=[('state', '=', 'valid')])

    extra_cost_ids = fields.One2many('erp_lodia.partner.extra_cost', 'partner_id')

    price_carry_coef = fields.Float(help="Coefficient applied to customer price when computing default carrier price\n"
                                         "Leave to 0.0 if it does not apply to this partner and disable altogether this feature")


class ResPartnerExtraCost(models.Model):
    _name = 'erp_lodia.partner.extra_cost'

    def _get_currency_id(self):
        return self.env.user.company_id.currency_id

    currency_id = fields.Many2one('res.currency', default=_get_currency_id, readonly=True)
    partner_id = fields.Many2one('res.partner', required=True)
    type_id = fields.Many2one('erp_lodia.transport.extra_cost.type', required=True)
    name = fields.Char(related='type_id.name', readonly=True)
    price = fields.Float(digits=dp.get_precision('Account'), required=True)

    @api.onchange('type_id')
    def _onchange_type_id(self):
        if self.type_id:
            self.price = self.type_id.default_price


class TransportLocation(models.Model):
    _name = 'erp_lodia.transport.location'
    _rec_name = 'display_name'

    display_name = fields.Char()
    lat = fields.Float(digits=dp.get_precision('Latitude/Longitude'))
    long = fields.Float(digits=dp.get_precision('Latitude/Longitude'))
    gmaps_address = fields.Char()

    @api.multi
    def distance_to(self, dest, method='tro'):
        if not method in TRANSPORT_LOCATION_DISTANCE_METHODS:
            raise exceptions.Warning(_("Invalid distance computation method `%s'") % method)
        return getattr(self, '_%s_distance_to' % method)(dest)

    @api.multi
    def _tro_distance_to(self, dest):
        """
        This method compute a distance from a location A to a location B using TRO methods based on a
        pre-computed table mapping each INSEE city to another.

        :param TransportLocation dest: the target location to compute the distance to
        :return int: the distance between the two locations
                     or 0 if target location is null
        """

        if not dest:
            return 0

        # self.env.cr.execute("""
        # SELECT r.km AS res
        # FROM transport_km_rel r
        # INNER JOIN transport_km_ville a ON a.code_tarifaire=r.code1
        # INNER JOIN transport_km_ville b ON b.code_tarifaire=r.code2
        # WHERE lower(a.city) LIKE '%%%s%%'
        # AND   lower(b.city) LIKE '%%%s%%'
        # LIMIT 1
        # """, (self.display_name, dest.display_name))
        #
        # return self.env.cr.fetchall()['res']

        A = self.env['transport.km_ville'].search([('city', '=', self.display_name.upper())])
        B = self.env['transport.km_ville'].search([('city', '=', dest.display_name.upper())])

        rel = self.env['transport.km_rel'].search([
            ('code1', '=', A.code_tarifaire),
            ('code2', '=', B.code_tarifaire),
        ])

        return rel.km

    @api.multi
    def _gmaps_distance_to(self, dest):
        """
        This method compute a distance from a location A to a location B
        using Google Maps' DistanceMatrix API.

        It also assign the field `gmap_address` of the corresponding location
        that has converted by DistanceMatrix from the `lat`/`long` values.

        :param TransportLocation dest: the target location to compute the distance to
        :return int: the distance between the two locations
                     or 0 if current location has no lat/long
                     or 0 if target location is null
                     or 0 if target location has no lat/long
        """
        if not self.lat or not self.long or not dest or not dest.lat or not dest.long:
            return 0

        args = (GMAPS_DISTANCEMATRIX_URL, self.lat, self.long, dest.lat, dest.long)
        resp = requests.get('%s/json?origins=%.4f,%.4f&destinations=%.4f,%.4f' % args).json()

        if resp.get('status') != 'OK':
            raise exceptions.Warning(_("Cannot get distance from Google Maps"))

        if not self.gmaps_address:
            self.write({'gmaps_address': resp['origin_addresses'][0]})
        if not dest.gmaps_address:
            dest.write({'gmaps_address': resp['destination_addresses'][0]})

        element = resp['rows'][0]['elements'][0]

        if element['status'] != 'OK':
            raise exceptions.Warning(_("Cannot get distance from Google Maps"))

        return element['distance']['value'] / 1000.0


class TroCity(models.Model):
    _name = 'transport.km_ville'
    _rec_name = 'city'

    city = fields.Char()
    ide = fields.Char()
    code_insee = fields.Char()
    code_tarifaire = fields.Char()

    @api.multi
    def name_get(self):
        return [
            (city.id, '%s - %s' % (city.city, city.code_insee))
            for city in self
        ]


class TroRel(models.Model):
    _name = 'transport.km_rel'

    code1 = fields.Char()
    code2 = fields.Char()
    km = fields.Integer()


class TransportGoodsType(models.Model):
    _name = 'erp_lodia.transport.goods.type'
    _rec_name = 'display_name'

    display_name = fields.Char()
    default_weight = fields.Float(digits=dp.get_precision('Transport Weight'),
                                  help='Weight in kilograms that will be used by default')


class TransportContainerType(models.Model):
    _name = 'erp_lodia.transport.container.type'
    _rec_name = 'name'

    name = fields.Char(required=True)


class TransportContainerCategory(models.Model):
    _name = 'erp_lodia.transport.container.category'
    _rec_name = 'name'

    name = fields.Char(required=True)


class TransportExtraCostType(models.Model):
    _name = 'erp_lodia.transport.extra_cost.type'
    _rec_name = 'name'

    name = fields.Char(required=True)
    default_price = fields.Float(digits=dp.get_precision('Account'), required=True)
    customer = fields.Boolean()


class TransportOrder(models.Model):
    _name = 'erp_lodia.transport.order'

    def _get_company_id(self):
        return self.env.user.company_id

    def _get_user_id(self):
        return self.env.user

    RO_CUSTOMER_INV = {
        'out_invoiced': [('readonly', True)],
        'in_invoiced': [('readonly', True)],
        'done': [('readonly', True)],
    }

    RO_SUPPLIER_INV = {
        'in_invoiced': [('readonly', True)],
        'done': [('readonly', True)],
    }

    company_id = fields.Many2one('res.company', default=_get_company_id)
    currency_id = fields.Many2one('res.currency')
    user_id = fields.Many2one('res.users', default=_get_user_id, required=True, copy=False,
                              help="Person in charge of this order")
    name = fields.Char(readonly=True, copy=False)
    location_id = fields.Many2one('erp_lodia.transport.location', required=True, states=RO_CUSTOMER_INV)
    location_dest_id = fields.Many2one('erp_lodia.transport.location', required=True, states=RO_CUSTOMER_INV)
    location_posit_id = fields.Many2one('erp_lodia.transport.location', required=True, states=RO_CUSTOMER_INV)
    transport_type = fields.Selection(TRANSPORT_TYPE, default='import', required=True, states=RO_CUSTOMER_INV)
    state = fields.Selection(TRANSPORT_ORDER_STATE, default='draft', readonly=True, copy=False)
    partner_id = fields.Many2one('res.partner', string='Customer', required=True, states=RO_CUSTOMER_INV,
                                 domain=[('customer', '=', True), ('is_company', '=', True)])
    partner_ref = fields.Char(related='partner_id.ref', readonly=True)
    partner_phone = fields.Char(related='partner_id.phone', readonly=True)
    partner_fax = fields.Char(related='partner_id.fax', readonly=True)
    partner_email = fields.Char(related='partner_id.email', readonly=True)
    delivery_partner_id = fields.Many2one('res.partner', string='Delivery Address', required=True, states=RO_CUSTOMER_INV,
                                          domain=[('customer', '=', True)])
    delivery_partner_street = fields.Char(related='delivery_partner_id.street', readonly=True)
    delivery_partner_street2 = fields.Char(related='delivery_partner_id.street2', readonly=True)
    delivery_partner_city = fields.Char(related='delivery_partner_id.city', readonly=True)
    delivery_partner_state_id = fields.Many2one('res.country.state', related='delivery_partner_id.state_id', readonly=True)
    delivery_partner_zip = fields.Char(related='delivery_partner_id.zip', readonly=True)
    delivery_partner_country_id = fields.Many2one('res.country', related='delivery_partner_id.country_id', readonly=True)
    boat = fields.Char(states=RO_CUSTOMER_INV)
    armament = fields.Char(states=RO_CUSTOMER_INV)
    date = fields.Date(default=fields.Date.context_today, required=True, states=RO_CUSTOMER_INV, copy=False)
    date_done = fields.Date(readonly=True, help="When the order has completed", copy=False)
    final_dest = fields.Char(string='Destination finale', states=RO_CUSTOMER_INV)
    customs_hint = fields.Text()
    comment_carry = fields.Text(states=RO_CUSTOMER_INV)
    comment = fields.Text(states=RO_CUSTOMER_INV)
    internal_comment = fields.Text()
    GO = fields.Char(states=RO_CUSTOMER_INV)
    tro_distance = fields.Float(digits=dp.get_precision('Distance'), compute='_set_distances', store=True,
                                help="Distance in kilometers")
    real_distance = fields.Float(digits=dp.get_precision('Distance'), compute='_set_distances', store=True,
                                 help="Distance in kilometers")
    partner_comment = fields.Text(related='partner_id.comment', readonly=True)


    price_equipments = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_extra_costs = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_customer_extra_costs = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_customer_const_extra_costs = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_carry = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_total_carry = fields.Float(digits=dp.get_precision('Account'), readonly=True, compute='_set_prices', store=True)
    price_customer = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_total_customer = fields.Float(digits=dp.get_precision('Account'), readonly=True, compute='_set_prices', store=True)

    gross_forecast = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)

    line_ids = fields.One2many('erp_lodia.transport.order.line', 'order_id', states=RO_CUSTOMER_INV)
    container_type_id = fields.Many2one('erp_lodia.transport.container.type', related='line_ids.container_type_id', readonly=True)
    mission_ids = fields.One2many('erp_lodia.transport.mission', 'order_id', states=RO_SUPPLIER_INV)
    document_ids = fields.Many2many('ir.attachment')
    equipment_ids = fields.One2many('erp_lodia.transport.mission.equipment', 'order_id', readonly=True)
    posit_location = fields.Char(related='location_posit_id.display_name')
    nr_of_lines = fields.Integer(compute='_set_nr_of_lines', store=True)

    name_partner = fields.Char(related='line_ids.name_partner')

    warning_check = fields.Boolean(compute='_warning_check', default=False)


    @api.one
    @api.depends('mission_ids.date_start')
    def _warning_check(self) :
        self.warning_check = any([x.date_start < self.date for x in self.mission_ids])

    @api.one
    @api.depends('location_id', 'location_dest_id')
    def _set_distances(self):
        self.tro_distance = 0.0
        self.real_distance = 0.0

        if self.location_id:
            self.tro_distance = self.location_id.distance_to(self.location_dest_id, method='tro')
            self.real_distance = self.location_id.distance_to(self.location_dest_id, method='gmaps')

    @api.one
    @api.depends('line_ids.price_carry_extra_costs', 'line_ids.price_equipments', 'line_ids.price_carry', 'line_ids.price_total_carry',
                 'line_ids.price_customer_extra_costs', 'line_ids.price_customer', 'line_ids.price_total_customer')
    def _set_prices(self):
        self.price_extra_costs = sum(self.line_ids.mapped('price_carry_extra_costs'))
        self.price_equipments = sum(self.line_ids.mapped('price_equipments'))
        self.price_carry = sum(self.line_ids.mapped('price_carry'))
        self.price_total_carry = sum(self.line_ids.mapped('price_total_carry'))

        self.price_customer_extra_costs = sum(self.line_ids.mapped('price_customer_extra_costs'))
        self.price_customer = sum(self.line_ids.mapped('price_customer'))
        self.price_total_customer = sum(self.line_ids.mapped('price_total_customer'))

        self.gross_forecast = self.price_total_customer - self.price_total_carry

    @api.one
    @api.depends('line_ids')
    def _set_nr_of_lines(self):
        self.nr_of_lines = len(self.line_ids)

    @api.onchange('company_id')
    def _onchange_company_id(self):
        self.currency_id = self.company_id.currency_id

    @api.onchange('transport_type')
    def _onchange_transport_type(self):
        self.tax_strategy = TRANSPORT_TYPE_TO_TAX_STRATEGY.get(self.transport_type)

    @api.onchange('name')
    def _onchange_name(self):
        self.name_partner = self.name

    @api.onchange('partner_id')
    def _onchange_partner_id(self):
        if self.partner_id:
            self.delivery_partner_id = self.partner_id.address_get(['delivery'])['delivery']

    @api.model
    def create(self, vals):
        if 'name' not in vals:
            vals['name'] = self.env.ref('erp_lodia.sequence_transport_orders')._next()

        return super(TransportOrder, self).create(vals)

    @api.one
    def write(self, vals):
        if vals.get('state') == 'done':
            vals['date_done'] = fields.Date.context_today(self)
        return super(TransportOrder, self).write(vals)

    @api.multi
    def btn_open_order(self):
        IMD = self.env['ir.model.data'].xmlid_to_res_id

        form_id = IMD('erp_lodia.form_transport_orders')

        if len(self) == 1:
            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'res_id': self.id,
                'view_id': form_id,
                'view_type': 'form',
                'view_mode': 'form',
            }
        else:
            list_id = IMD('erp_lodia.list_transport_orders')

            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'domain': [('id', 'in', self.ids)],
                'views': [(list_id, 'list'), (form_id, 'form')],
                'view_type': 'form',
                'view_mode': 'list,form',
            }

    @api.multi
    def btn_make_order(self):
        self.write({'state': 'draft'})
        return self.btn_open_order()

    @api.multi
    def btn_validate(self):
        mission_names = dict(self.env['erp_lodia.transport.mission'].fields_get()['mission']['selection'])

        for mission in self.mission_ids:
            if not mission.partner_id:
                mission_name = mission_names[mission.mission]

                raise exceptions.ValidationError(_("Please set a driver in %s of container %s") %
                                                 (mission_name, mission.order_line_id.number))

        self.write({'state': 'valid'})
        self.mapped('line_ids').mapped('mission_ids').assign_name()
        return True

    @api.multi
    def btn_back_to_draft(self):
        return self.write({'state': 'draft'})

    @api.multi
    def btn_done(self):
        self.write({'state': 'done'})


class TransportOrderLine(models.Model):
    _name = 'erp_lodia.transport.order.line'
    _rec_name = 'display_name'

    order_id = fields.Many2one('erp_lodia.transport.order', readonly=True, ondelete='cascade')
    currency_id = fields.Many2one('res.currency', related='order_id.currency_id')
    state = fields.Selection(TRANSPORT_ORDER_STATE, related='order_id.state', readonly=True)
    container_type_id = fields.Many2one('erp_lodia.transport.container.type', required=True, states=TransportOrder.RO_CUSTOMER_INV)
    container_category_id = fields.Many2one('erp_lodia.transport.container.category', states=TransportOrder.RO_CUSTOMER_INV)
    goods_type_id = fields.Many2one('erp_lodia.transport.goods.type', states=TransportOrder.RO_CUSTOMER_INV)
    weight = fields.Float(digits=dp.get_precision('Transport Weight'),
                          help='Weight in kilograms')
    number = fields.Char(states=TransportOrder.RO_CUSTOMER_INV)
    seal_number = fields.Char()
    booking = fields.Char(states=TransportOrder.RO_CUSTOMER_INV)
    conveyance = fields.Char()
    date_start = fields.Date(states=TransportOrder.RO_CUSTOMER_INV, help="When the container become available and ready to get from the port")
    date_end = fields.Date(states=TransportOrder.RO_CUSTOMER_INV, help="When the container must be delivered to the port")
    location_id = fields.Many2one('erp_lodia.transport.location', states=TransportOrder.RO_CUSTOMER_INV)
    customs_number = fields.Char(states=TransportOrder.RO_CUSTOMER_INV)

    price_customer = fields.Float(digits=dp.get_precision('Account'), states=TransportOrder.RO_CUSTOMER_INV)
    price_customer_extra_costs = fields.Float(digits=dp.get_precision('Account'), compute='_set_customer_prices', store=True)
    price_total_customer = fields.Float(digits=dp.get_precision('Account'), compute='_set_customer_prices', store=True)

    price_carry = fields.Float(digits=dp.get_precision('Account'), compute='_set_carry_prices', store=True)
    price_equipments = fields.Float(digits=dp.get_precision('Account'), compute='_set_carry_prices', store=True)
    price_carry_extra_costs = fields.Float(digits=dp.get_precision('Account'), compute='_set_carry_prices', store=True)
    price_total_carry = fields.Float(digits=dp.get_precision('Account'), compute='_set_carry_prices', store=True)

    carrier_in_id = fields.Many2one('res.partner', compute='_set_carriers')
    carrier_out_id = fields.Many2one('res.partner', compute='_set_carriers')

    mission_ids = fields.One2many('erp_lodia.transport.mission', 'order_line_id')

    delivery_mission_id = fields.Many2one('erp_lodia.transport.mission', compute='_set_delivery_mission_id', store=True)
    delivery_partner_id = fields.Many2one('res.partner', related='delivery_mission_id.partner_id', store=True, readonly=True)
    date_delivery = fields.Datetime(related='delivery_mission_id.date_start', store=True, readonly=True)

    display_name = fields.Char(compute='_set_display_name')

    number_partner = fields.Char(related='delivery_mission_id.name_partner',
                                 help="Reference of the delivery mission used by carriers in their own systems")

    name_partner = fields.Char(help="Reference for this container used by customers in their own systems")

    @api.constrains('mission_ids')
    def _missions_must_be_unique_by_their_types(self):
        for (mission, mission_label) in TRANSPORT_MISSION:
            nr_of_missions = len(self.mission_ids.filtered(lambda x: x.mission == mission))
            if nr_of_missions > 1:
                raise exceptions.ValidationError(_("You assigned the mission \"%s\" more than once on container %s") % (mission_label, self.container_type_id.name))

    @api.one
    @api.depends('number', 'container_type_id.name')
    def _set_display_name(self):
        self.display_name = '(%s) %s' % (self.container_type_id.name, self.number or 'N/A')

    @api.one
    @api.depends('order_id.mission_ids.carrier_id', 'order_id.mission_ids.mission')
    def _set_carriers(self):
        missions = self.order_id.mission_ids
        self.carrier_in_id = missions.filtered(lambda x: x.mission == 'in').carrier_id
        self.carrier_out_id = missions.filtered(lambda x: x.mission == 'out').carrier_id

    @api.one
    @api.depends('mission_ids.mission')
    def _set_delivery_mission_id(self):
        self.delivery_mission_id = self.mission_ids.filtered(lambda x: x.mission == 'delivery')
        if len(self.delivery_mission_id) > 1:
            self.delivery_mission_id = self.delivery_mission_id[0]

    @api.one
    @api.depends('price_customer', 'mission_ids.extra_cost_ids.price', 'mission_ids.extra_cost_ids.customer')
    def _set_customer_prices(self):
        self.price_customer_extra_costs = sum(self.mission_ids.mapped('extra_cost_ids').filtered(lambda x: x.customer).mapped('price'))
        self.price_total_customer = self.price_customer + self.price_customer_extra_costs

    @api.one
    @api.depends('mission_ids.price_carry', 'mission_ids.price_equipments', 'mission_ids.price_extra_costs')
    def _set_carry_prices(self):
        self.price_carry = sum(self.mission_ids.mapped('price_carry'))
        self.price_equipments = sum(self.mission_ids.mapped('price_equipments'))
        self.price_carry_extra_costs = sum(self.mission_ids.mapped('price_extra_costs'))
        self.price_total_carry = sum(self.mission_ids.mapped('price_total_carry'))

    @api.one
    @api.depends('number', 'mission_ids.mission', 'mission_ids.name_partner')
    def _set_number_partner(self):
        mission = self.mission_ids.filtered(lambda x: x.mission == 'delivery')
        self.number_partner = mission.name_partner if mission else self.number

    @api.model
    def name_search(self, name='', args=None, operator='ilike', limit=100):
        forward_fields = [
            'container_type_id.name',
            'number',
            'booking',
        ]

        # forward search to children fields
        domain = []
        for i in range(0, len(forward_fields) - 1):
            domain.append('|')
        for field in forward_fields:
            domain.append((field, operator, name))

        # narrow search to an order if asked
        if 'search_narrow_to_order_id' in self.env.context:
            order_id = self.env.context['search_narrow_to_order_id']
            domain.insert(0, ('order_id', '=', order_id))

        return self.search(domain, limit=limit).name_get()

    @api.one
    @api.onchange('goods_type_id')
    def _onchange_goods_type_id(self):
        if not self.weight:
            self.weight = self.goods_type_id.default_weight

    @api.multi
    def btn_open(self):
        IMD = self.env['ir.model.data'].xmlid_to_res_id

        form_id = IMD('erp_lodia.form_transport_order_lines')

        if len(self) == 1:
            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'res_id': self.id,
                'view_id': form_id,
                'view_type': 'form',
                'view_mode': 'form',
            }
        else:
            raise exceptions.Warning("not implemented")  # TODO
            # list_id = IMD('erp_lodia.list_transport_order_lines')
            #
            # return {
            #     'type': 'ir.actions.act_window',
            #     'res_model': self._name,
            #     'domain': [('id', 'in', self.ids)],
            #     'views': [(list_id, 'list'), (form_id, 'form')],
            #     'view_type': 'form',
            #     'view_mode': 'list,form',
            # }


class TransportMission(models.Model):
    _name = 'erp_lodia.transport.mission'

    type = fields.Char(states=TransportOrder.RO_SUPPLIER_INV)
    order_id = fields.Many2one('erp_lodia.transport.order', ondelete='cascade')
    order_line_id = fields.Many2one('erp_lodia.transport.order.line', ondelete='cascade', required=True)
    currency_id = fields.Many2one('res.currency', related='order_id.currency_id')
    partner_id = fields.Many2one('res.partner', domain=[('driver', '=', True)])
    carrier_id = fields.Many2one('res.partner', related='partner_id.parent_id', readonly=True, store=True)
    company_id = fields.Many2one('res.company', related='order_id.company_id', readonly=True, store=True)
    order_partner_id = fields.Many2one('res.partner', related='order_id.partner_id', readonly=True, store=True)
    mission = fields.Selection(TRANSPORT_MISSION, required=True, states=TransportOrder.RO_SUPPLIER_INV)
    name = fields.Char(readonly=True)
    name_partner = fields.Char()
    date_validity_start = fields.Date(related='order_line_id.date_start', readonly=True)
    date_validity_end = fields.Date(related='order_line_id.date_end', readonly=True)
    date_start = fields.Datetime(states=TransportOrder.RO_SUPPLIER_INV)
    date_end = fields.Datetime(states=TransportOrder.RO_SUPPLIER_INV)
    state = fields.Selection(TRANSPORT_ORDER_STATE, related='order_line_id.order_id.state', readonly=True)
    truck_number = fields.Char(states=TransportOrder.RO_SUPPLIER_INV)
    trailer_number = fields.Char(states=TransportOrder.RO_SUPPLIER_INV)
    location_id = fields.Many2one('erp_lodia.transport.location', states=TransportOrder.RO_SUPPLIER_INV)
    location_dest_id = fields.Many2one('erp_lodia.transport.location', states=TransportOrder.RO_SUPPLIER_INV)
    armament = fields.Char(related='order_id.armament', readonly=True)
    final_dest = fields.Char(related='order_id.final_dest', readonly=True)
    number = fields.Char(related='order_line_id.number', readonly=True)
    boat = fields.Char(related='order_id.boat', readonly=True)
    weight = fields.Float(related='order_line_id.weight', digits=dp.get_precision('Transport Weight'), readonly=True)
    reference = fields.Char(string='Référence Chargement/livraison')
    order_number = fields.Char(related='order_id.name', readonly=True)

    price_carry = fields.Float(digits=dp.get_precision('Account'), states=TransportOrder.RO_SUPPLIER_INV)
    price_equipments = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_extra_costs = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)
    price_total_carry = fields.Float(digits=dp.get_precision('Account'), compute='_set_prices', store=True)

    equipment_ids = fields.One2many('erp_lodia.transport.mission.equipment', 'mission_id', states=TransportOrder.RO_SUPPLIER_INV)
    extra_cost_ids = fields.One2many('erp_lodia.transport.order.extra_cost', 'mission_id', states=TransportOrder.RO_SUPPLIER_INV)

    tro_distance = fields.Float(digits=dp.get_precision('Distance'), compute='_set_distances', store=True)
    real_distance = fields.Float(digits=dp.get_precision('Distance'), compute='_set_distances', store=True)

    @api.one
    @api.depends('price_carry', 'equipment_ids.price', 'extra_cost_ids.price', 'extra_cost_ids.customer')
    def _set_prices(self):
        self.price_equipments = sum(self.equipment_ids.mapped('price'))
        self.price_extra_costs = sum(self.extra_cost_ids.filtered(lambda x: not x.customer).mapped('price'))
        self.price_total_carry = self.price_carry + self.price_equipments + self.price_extra_costs

    @api.one
    @api.depends('location_id', 'location_dest_id')
    def _set_distances(self):
        self.tro_distance = self.real_distance = 0.0

        if self.location_id:
            self.tro_distance = self.location_id.distance_to(self.location_dest_id, method='tro')
            self.real_distance = self.location_id.distance_to(self.location_dest_id, method='gmaps')

    @api.constrains('date_start', 'date_end')
    @api.depends('date_validity_start', 'date_validity_end')
    def _date_must_be_in_order_line_bounds(self):
        date_validity_start = self.date_validity_start and fields.Date.from_string(self.date_validity_start)
        date_validity_end = self.date_validity_end and fields.Date.from_string(self.date_validity_end)
        mission_label = self.mission and [x[1] for x in TRANSPORT_MISSION if x[0] == self.mission][0]

        def _bounds(label, d):
            if not d:
                return

            if date_validity_start and d < date_validity_start:
                raise exceptions.ValidationError(_("Mission \"%s\" must have a %s date after %s's start")
                                                 % (mission_label, label, self.order_line_id.container_type_id.name))

            if date_validity_end and d > date_validity_end:
                raise exceptions.ValidationError(_("Mission \"%s\" must have a %s date before %s's end")
                                                 % (mission_label, label, self.order_line_id.container_type_id.name))

        date_start = self.date_start and fields.Date.from_string(self.date_start)
        date_end = self.date_end and fields.Date.from_string(self.date_end)

        _bounds('start', date_start)
        _bounds('end', date_end)

        if date_start and date_end and date_start > date_end:
            raise exceptions.ValidationError(_("Mission \"%s\" of %s must have a start date that is before its end date")
                                             % (mission_label, self.order_line_id.container_type_id.name))

    @api.onchange('mission', 'partner_id')
    def _onchange_mission(self):
        self.price_carry = 0

        if self.mission == 'delivery':
            coef = self.partner_id.parent_id.price_carry_coef
            if coef:
                self.price_carry = coef * self.order_line_id.price_total_customer

    @api.onchange('order_id')
    def _onchange_order_id(self):
        if self.order_id:
            self.location_id = self.order_id.location_id
            self.location_dest_id = self.order_id.location_dest_id

    @api.one
    def assign_name(self):
        if not self.name:
            self.name = self.env.ref('erp_lodia.sequence_transport_missions')._next()

    @api.multi
    def btn_open(self):
        IMD = self.env['ir.model.data'].xmlid_to_res_id
        form_id = IMD('erp_lodia.form_transport_missions')

        if len(self) == 1:
            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'res_id': self.id,
                'view_id': form_id,
                'view_type': 'form',
                'view_mode': 'form',
            }
        else:
            list_id = IMD('erp_lodia.list_transport_missions')

            return {
                'type': 'ir.actions.act_window',
                'res_model': self._name,
                'domain': [('id', 'in', self.ids)],
                'views': [(list_id, 'list'), (form_id, 'form')],
                'view_type': 'form',
                'view_mode': 'list,form',
            }

    @api.one
    def name_get(self):
        # noinspection PyArgumentList
        mission = dict(self.fields_get()['mission']['selection'])[self.mission]

        result = ''
        if self.name:
            result += '%s ' % self.name

        result += _('%s by %s') % (mission, self.partner_id.name)

        return self.id, result


class TransportOrderExtraCost(models.Model):
    _name = 'erp_lodia.transport.order.extra_cost'
    _rec_name = 'name'

    mission_id = fields.Many2one('erp_lodia.transport.mission', ondelete='cascade')
    order_id = fields.Many2one('erp_lodia.transport.order', related='mission_id.order_id', store=True, readonly=True)
    type_id = fields.Many2one('erp_lodia.transport.extra_cost.type', required=True)
    name = fields.Char(related='type_id.name')
    currency_id = fields.Many2one('res.currency', related='order_id.currency_id')
    price = fields.Float(digits=dp.get_precision('Account'), required=True)
    customer = fields.Boolean()

    @api.onchange('type_id')
    def _onchange_type_id(self):
        if self.type_id:
            self.customer = self.type_id.customer
            self.price = self.type_id.default_price

        current_partner_id = self.env.context.get('current_partner_id')
        if current_partner_id:
            current_partner = self.env['res.partner'].browse(current_partner_id)
            partner_prices_by_type_id = {x.type_id.id: x.price for x in current_partner.extra_cost_ids}
            if self.type_id.id in partner_prices_by_type_id:
                self.price = partner_prices_by_type_id[self.type_id.id]


class TransportEquipment(models.Model):
    _inherit = 'erp_lodia.equipment'

    order_equipment_ids = fields.One2many('erp_lodia.transport.mission.equipment', 'equipment_id')
    has_overlapping_orders = fields.Boolean(compute='_set_has_overlapping_orders', store=True)

    @api.one
    @api.depends('order_equipment_ids.date_start', 'order_equipment_ids.date_end')
    def _set_has_overlapping_orders(self):
        # naively detect overlaps with a O(n²) algorithm
        # using a sorted list might provide a speed-up tho

        class CalendarSpan:
            """
            :type start: datetime.date
            :type end: datetime.date
            """
            def __init__(self, start, end):
                assert start <= end
                self.start = start
                self.end = end

            def overlaps_with(self, other):
                """
                :param CalendarSpan other:
                :return bool:
                """
                return self.end > other.start

        class Calendar:
            """
            :type spans: list[CalendarSpan]
            """
            def __init__(self):
                self.spans = []

            def push(self, start, end):
                """
                :param datetime.date start:
                :param datetime.date end:
                :return bool:
                """
                cur_span = CalendarSpan(start, end)
                for span in self.spans:
                    if span.overlaps_with(cur_span):
                        return False
                self.spans.append(cur_span)
                return True


        self.has_overlapping_orders = False

        calendar = Calendar()

        for order in self.order_equipment_ids:
            if not order.date_start or not order.date_end:
                continue

            date_start = fields.Date.from_string(order.date_start)
            date_end = fields.Date.from_string(order.date_end)

            if not calendar.push(date_start, date_end):
                self.has_overlapping_orders = True
                break


class TransportMissionEquipment(models.Model):
    _name = 'erp_lodia.transport.mission.equipment'

    mission_id = fields.Many2one('erp_lodia.transport.mission', ondelete='cascade')
    order_line_id = fields.Many2one('erp_lodia.transport.order.line', related='mission_id.order_line_id', store=True)
    order_id = fields.Many2one('erp_lodia.transport.order', related='mission_id.order_id', store=True)
    equipment_id = fields.Many2one('erp_lodia.equipment', required=True)
    equipment_type_id = fields.Many2one('erp_lodia.equipment.type', related='equipment_id.type_id', readonly=True)
    currency_id = fields.Many2one('res.currency', related='order_id.currency_id', readonly=True)
    price = fields.Float(digits=dp.get_precision('Account'), default=0, required=True)
    date_start = fields.Datetime(related='mission_id.date_start')
    date_end = fields.Datetime(related='mission_id.date_end')

    @api.one
    def name_get(self):
        return self.id, self.equipment_type_id.name_get()[0][1]
